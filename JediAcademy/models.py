# coding: utf-8

from django.db import models


class Planet(models.Model):
    name = models.CharField(verbose_name='Наименование', max_length=255)

    def __str__(self):
        return self.name


class Jedi(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=254)
    planet = models.ForeignKey(Planet, verbose_name='Планета на которой он обучает', on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.name, self.planet)


class Candidate(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=254)
    planet = models.ForeignKey(Planet, verbose_name='Планета обитания', on_delete=models.CASCADE)
    age = models.IntegerField(verbose_name='Возраст', default=0)
    email = models.EmailField(verbose_name='Email')
    jedi = models.ForeignKey(Jedi, blank=True, null=True, verbose_name='Джедай у которого в падаванах')

    def __str__(self):
        return self.name


class TestQuestion(models.Model):
    questions_text = models.TextField(verbose_name='Список вопросов')
    institution_id = models.BigIntegerField(verbose_name='Код ордена', unique=True)

    def __str__(self):
        return '{} {}'.format(self.institution_id, self.questions_text[:32])


class Answer(models.Model):
    question = models.ForeignKey(TestQuestion, verbose_name='Вопрос')
    answer = models.BooleanField(verbose_name='Ответ')
    candidate = models.ForeignKey(Candidate, verbose_name='Кандидат')

    def __str__(self):
        return '{} - {} : {}'.format(self.candidate, self.question.questions_text[:32], self.answer)
