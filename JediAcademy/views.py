# coding: utf-8

from django.db.models import Count
from django.shortcuts import render, redirect

from JediAcademy.forms import AddCandidateForm, SelectJediForm, PadavanAnswersForm
from JediAcademy.models import Jedi, Answer, TestQuestion


def index(request):
    return render(request, 'main_form.html')


def new_candidate(request):
    if request.method == "POST":
        form = AddCandidateForm(request.POST)
        if form.is_valid():
            candidate = form.save()
            return redirect('questions', candidate.id)
    else:
        form = AddCandidateForm()

    return render(request, 'new_candidate_form.html', {'form': form})


def jedi_select(request):
    if request.method == "POST":
        form = SelectJediForm(request.POST)
        if form.is_valid():
            jedi = form.cleaned_data["name"]
            return redirect('candidate_check', jedi.id)
    else:
        form = SelectJediForm()

    return render(request, 'jedi_form.html', {'form': form})


def questions(request, candidate_id=None):
    questions = TestQuestion.objects.all()
    if request.method == 'POST':
        for question in questions:
            answer, created = Answer.objects.get_or_create(candidate_id=candidate_id, question=question, answer=False)
            answer.answer = bool(request.POST.get(str(question.id), False))
            answer.save()

        return redirect('main_form')

    return render(request, 'questions_form.html', {'questions': questions, 'candidate_id': candidate_id})


def candidate_check(request, jedi_id=None):
    jedi = Jedi.objects.get(id=jedi_id)
    answers = Answer.objects.filter(candidate__planet=jedi.planet, candidate__jedi=None).all()

    if request.method == "POST":
        form = PadavanAnswersForm(request.POST, jedi=jedi)
        if form.is_valid():
            form.save()
            return redirect('main_form')
    else:
        form = PadavanAnswersForm(jedi=jedi)

    return render(request, 'check_candidate_form.html',
                  {'form': form, 'answers': answers, 'jedi_id': jedi_id})


def jedi_list_w_padavans(request):
    jedis = Jedi.objects \
        .prefetch_related('candidate_set') \
        .filter(candidate__isnull=False) \
        .values('id', 'name') \
        .annotate(counts=Count('candidate__id'))

    return render(request, 'jedi_list.html', {'jedis': jedis})


def jedi_full_list(request):
    jedis = Jedi.objects \
        .prefetch_related('candidate_set') \
        .values('id', 'name') \
        .annotate(counts=Count('candidate__id')) \
        .order_by('-counts')

    return render(request, 'jedi_list.html', {'jedis': jedis})
