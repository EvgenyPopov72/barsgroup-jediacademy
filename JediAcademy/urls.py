# coding: utf-8

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='main_form'),
    url(r'^new_candidate$', views.new_candidate, name='new_candidate'),
    url(r'^jedi_select$', views.jedi_select, name='jedi_select'),
    url(r'^candidate_check/(?P<jedi_id>[0-9]+)$', views.candidate_check, name='candidate_check'),
    url(r'^questions/(?P<candidate_id>[0-9]+)$', views.questions, name='questions'),
    url(r'^jedi_list_w_padavans', views.jedi_list_w_padavans, name='jedi_list_w_padavans'),
    url(r'^jedi_full_list', views.jedi_full_list, name='jedi_full_list'),
]