from django.contrib import admin

from JediAcademy.models import Jedi, Planet, TestQuestion, Candidate

# Register your models here.

admin.site.register(Jedi)
admin.site.register(Planet)
admin.site.register(TestQuestion)
admin.site.register(Candidate)
