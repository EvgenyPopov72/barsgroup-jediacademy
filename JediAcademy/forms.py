# coding: utf-8
from threading import Thread

from django import forms
from django.conf import settings
from django.core.mail import send_mail

from JediAcademy.mail_util import async_send_mail
from .models import Candidate, Jedi


class AddCandidateForm(forms.ModelForm):
    class Meta:
        model = Candidate
        exclude = ('jedi',)


class SelectJediForm(forms.Form):
    name = forms.ModelChoiceField(queryset=Jedi.objects, label='select your name', empty_label=None)


class PadavanAnswersForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.jedi = kwargs.pop('jedi')
        super().__init__(*args, **kwargs)
        self.fields['candidate'].queryset = Candidate.objects \
            .filter(planet=self.jedi.planet_id, jedi=None, answer__isnull=False) \
            .distinct()

    def save(self, commit=True):
        padavan_count = Candidate.objects.filter(jedi=self.jedi).count()
        if padavan_count <= 3:
            padavan = self.cleaned_data['candidate']
            padavan.jedi = self.jedi
            padavan.save()

            thr = Thread(target=async_send_mail, args=[padavan.email])
            thr.start()

    candidate = forms.ModelChoiceField(queryset=None, label='select your padavan', empty_label=None)
