FROM alpine:latest
LABEL maintainer="Evgeny Popov<evgeny.popov72@gmail.com>"

# setup app folders
RUN mkdir /app
WORKDIR /app
ADD . /app

RUN apk update && apk upgrade && \

    apk add --update --no-cache \
    python3 \
    python3-dev \
    postgresql-dev \
    gcc g++ linux-headers && \
    pip3 install -U pip && \

    pip3 install --no-cache-dir -r /app/requirements.txt && rm -r /root/.cache && \

# Clean
    apk del -r python3-dev gcc g++ linux-headers

RUN chown -R nobody:nobody /app
USER nobody
EXPOSE 8000
ENV DATABASE_URL=${DATABASE_URL}

#ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:8000", "barsgroup.wsgi"]
#CMD ["gunicorn", "--bind", "0.0.0.0:5000", "barsgroup.wsgi"]
CMD gunicorn --bind 0.0.0.0:$PORT barsgroup.wsgi